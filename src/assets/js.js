function ColorGame() {
  // Game Initializer
  var squares,
    colors,
    pickedColor

  var ezMode = false
  var primaryColor = 'steelblue'

// Buttons
  var ezBtn = document.querySelector('#ezBtn')
  var hrdBtn = document.querySelector('#hrdBtn')
  var resetBtn = document.querySelector('#resetBtn')

// Event Listeners
  ezBtn.addEventListener('click', function () {
    hrdBtn.classList.remove('selected')
    ezBtn.classList.add('selected')
    ezMode = true
    restartGame()
  })

  hrdBtn.addEventListener('click', function () {
    ezBtn.classList.remove('selected')
    hrdBtn.classList.add('selected')
    ezMode = false
    restartGame()
  })

  resetBtn.addEventListener('click', restartGame)

// Element Selectors
  var h1 = document.querySelector('h1')

  function selectSquares () {
    squares = document.querySelectorAll('.square')
  }

  function restartGame () {
    selectSquares()
    generateColors()
    pickColor()
    rgbInformer()
    updateSquares()
    displayMessage()
    h1.style.backgroundColor = ''
    resetBtn.textContent = 'New Colors'
  }

  function displayMessage (str) {
    document.getElementById('message').textContent = str
  }

  function rgbInformer () {
    document.getElementById('colorDisplay').textContent = pickedColor.toUpperCase()
  }

  function updateSquares () {
    // Loop through all squares
    for (var i = 0; i < squares.length; i++) {
      if (colors[i]) {
        squares[i].style.backgroundColor = colors[i]
      } else {
        squares[i].style.backgroundColor = '#232323'
      }
    }
  }

  function changeColors (color) {
    // Loop through all squares
    for (var i = 0; i < colors.length; i++) {
      squares[i].style.backgroundColor = color
    }
  }

  function pickColor () {
    pickedColor = colors[Math.floor(Math.random() * colors.length)]
  }

  function generateColors () {
    var arr

    if (ezMode) {
      arr = new Array(3)
    } else {
      arr = new Array(squares.length)
    }

    // Add num random colors to array
    for (var i = 0; i < arr.length; i++) {
      // Get random color and push into array
      arr[i] = randomColor()
    }

    colors = arr
  }

  function randomColor () {
    // Pick "red" value
    var r = Math.floor(Math.random() * 256)
    // Pick "green" value
    var g = Math.floor(Math.random() * 256)
    // Pick "blue" value
    var b = Math.floor(Math.random() * 256)

    return 'rgb(' + r + ',' + g + ',' + b + ')'
  }

  restartGame()

  for (var i = 0; i < squares.length; i++) {
    // Add Click Listener to Squares
    squares[i].addEventListener('click', function () {
      // Grab color of clicked square
      var clickedColor = this.style.backgroundColor.replace(/ /g, '')
      if (clickedColor === pickedColor) {
        displayMessage('Correct!')
        changeColors(clickedColor)
        resetBtn.textContent = 'Play Again?'
        h1.style.backgroundColor = clickedColor
      } else {
        this.style.backgroundColor = '#232323'
        displayMessage('Try Again!')
      }
    })
  }


}

module.exports = ColorGame;
