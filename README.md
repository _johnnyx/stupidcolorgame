**Welcome to the StupidColor Game!**

This game pretty much has zero purpose except for showcasing the most bask of JS abilities. As you progress through this portfolio you will see just how much more advanced these projects get.

In the meantime, enjoy this ridiculous game by cloning the repo or visiting the website above. For now it is a static website on an S3 bucket 

---

## Launch Game

You�ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Make sure to either download or clone this repo(steps below for help cloning).
2. Once you have all the files in one folder, double click on **index.html**
3. Have fun and watch time as it slips away and away into the abyss...

---

## Clone a repository

Hey there, so you're not sure how to clone a repo? No sweat, you'll need GIT installed on your machine and lucky for you they have installers made [here](https://git-scm.com/downloads "GIT Installers") :)

1. Now that we have GIT installed we can clone the repo with this command
`git clone https://bitbucket.org/_johnnyx/stupidcolorgame.git`
